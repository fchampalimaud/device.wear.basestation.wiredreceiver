// Author: Champalimaud Foundation's Hardware platform

#include "cpu_1v1.h"
#define F_CPU 32000000
#include <util/delay.h>

// Define here the values that will be saved into the EEPROM
#define DAC_GAINCAL 0x83
#define DAC_OFFSETCAL 0x85


#define set_LED clear_io(PORTD, 3)
#define clr_LED set_io(PORTD, 3)
#define tgl_LED toogle_io(PORTD, 3)

void init_pins(void)
{
	io_pin2out(&PORTD, 3, OUT_IO_DIGITAL, IN_EN_IO_DIS);
	set_LED;
	
	/* Initialize DACB channel 0 */
	DACB.CTRLB = 0;
	DACB.CTRLC = DAC_REFSEL_AREFA_gc;
	DACB.CTRLA = DAC_CH0EN_bm | DAC_ENABLE_bm;
	DACB.CH0OFFSETCAL = 0;
	DACB.CH0GAINCAL = 0;
}

void burn_calibration(void)
{
	eeprom_wr_byte(0, 0xAA);
	eeprom_wr_byte(1, 0xBB);
	eeprom_wr_byte(2, DAC_GAINCAL);
	eeprom_wr_byte(3, DAC_OFFSETCAL);
}

int main(void)
{
	cpu_config_clock(F_CPU, true, true);
	_delay_ms(200);
	init_pins();
	burn_calibration();

	/* Set DAC value */
	while(1)
	{		
		DACB_CH0DATA = 4096-1;	// Full-scale
		set_LED;
		
		_delay_ms(10000);
		
		DACB_CH0DATA = 0x800;	// Half-scale
		clr_LED;
		
		_delay_ms(10000);
	}
}