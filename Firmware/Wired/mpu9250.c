#include "mpu9250.h"
#include <string.h>

//#define XMEGA
#define NRF

#ifdef XMEGA
	#include "i2c.h"
	#define F_CPU 32000000
	#include <util/delay.h>
#else
	#include <stdint.h>
	#include <stdbool.h>
	#include <reg24le1.h>
	#include "..\nRF24LE1\nRF_i2c.h"
	#include "..\libs\nRF24LE1\nRF_delays.h"

	#define i2c0_wReg(a) nRF_i2c_wReg(a)
	#define i2c0_rReg(a,b) nRF_i2c_rReg(a,b)

	#define _delay_ms aux_wait_ms
#endif


/************************************************************************/
/* Specific register functions                                          */
/************************************************************************/
bool mpu9250_exist(i2c_dev_t dev)
{
	dev.reg = WHO_AM_I;
	i2c0_rReg(&dev, 1);
	return (dev.reg_val == GM_WHO_AM_I ) ? true : false;
}

void mpu9250_reset(i2c_dev_t dev)
{
	dev.reg = PWR_MGMT_1;
	dev.reg_val = B_H_RESET;
	i2c0_wReg(&dev);
}

void mpu9250_sleep(i2c_dev_t dev)
{
	dev.reg = PWR_MGMT_1;
	dev.reg_val = B_SLEEP;
	i2c0_wReg(&dev);
}

bool mpu9250_check_raw_data_rdy_int(i2c_dev_t dev)
{
	dev.reg = INT_STATUS;
	i2c0_rReg(&dev, 1);
	return ((dev.reg_val & 0xA7) == B_RAW_DATA_RDY_INT) ? true : false;
}

void mpu9250_enable_i2c_bypass(i2c_dev_t dev)
{
	dev.reg = INT_PIN_CFG;
	dev.reg_val = B_BYPASS_EN;
	i2c0_wReg(&dev);
}

void mpu9250_disable_i2c_bypass(i2c_dev_t dev)
{
	dev.reg = INT_PIN_CFG;
	dev.reg_val = 0;
	i2c0_wReg(&dev);
}

void mpu9250_mag_trig_single_meas(i2c_dev_t dev)
{
	uint8_t mpu9250_add = dev.add;	// Save MPU9250 address
	dev.add = 0x0C;						// Set AK8975 address
	
	dev.reg = CNTL1;
	dev.reg_val = GM_SINGLE_MEAS | B_BIT;
	i2c0_wReg(&dev);
	
	dev.add = mpu9250_add;				// Recover MPU9250 address
}

bool mpu9250_mag_check_drdy(i2c_dev_t dev)
{
	uint8_t mpu9250_add = dev.add;	// Save MPU9250 address
	dev.add = 0x0C;						// Set AK8975 address

	dev.reg = ST1;
	i2c0_rReg(&dev, 1);
	
	dev.add = mpu9250_add;				// Recover MPU9250 address
	
	return (dev.reg_val & B_DRDY) ? true : false;	
}


/************************************************************************/
/* Start acquisition                                                    */
/************************************************************************/
bool mpu9250_start(
	i2c_dev_t dev,
	uint16_t freq,
	uint8_t use_acc,
	uint8_t use_gyr,
	uint8_t use_mag,
	uint8_t range_acc,
	uint16_t range_gyr)
{
	bool success = true;

	uint8_t dlp_cfg = 1;
	uint8_t smplrt_div = 1;
	
	/* Save MPU9250 address*/
	uint8_t mpu9250_add = dev.add;

	/* Configure Power Mode and Clock Source */
	// SLEEP = 0			No sleep mode
	// CYCLE = 0			Cycle between sleep and acquire sample is not used
	// PD_PTAT = 0 		Temperature sensor is enabled
	// CLKSEL = 0			Internal 20MHz oscillator
	dev.reg = PWR_MGMT_1;
	dev.reg_val = GM_CLKSEL_INT20MHz;
	i2c0_wReg(&dev);
	i2c0_rReg(&dev, 1);
	if (dev.reg_val != GM_CLKSEL_INT20MHz)
		success = false;

	/* Delay of 100 ms to make sure PLL is established */
	_delay_ms(100);

	/* Select best available clock source */
	dev.reg_val = GM_CLKSEL_BEST;
	i2c0_wReg(&dev);
	i2c0_rReg(&dev, 1);
	if (dev.reg_val != GM_CLKSEL_BEST)
		success = false;

	/* Check if magnetometer is available */
	if (use_mag)
	{
		/* Enable I2C Bypass Mode */
		mpu9250_enable_i2c_bypass(dev);
			
		/* AK8975 address*/
		dev.add = 0x0C;
			
		/* Read AK8975 Who Am I (WIA) */
		dev.reg = WIA;
		i2c0_rReg(&dev, 1);
		if (dev.reg_val != GM_WIA)
			success = false;
			
		/* Disable I2C Bypass Mode */
		mpu9250_disable_i2c_bypass(dev);
	}
		
	/* Recover MPU9250 address*/
	dev.add = mpu9250_add;

	/* Update PWR_MGMT_2 content if gyroscope if not used*/
	dev.reg_val = 0;
	if (!use_acc)
		dev.reg_val |= B_DISABLE_XA | B_DISABLE_YA | B_DISABLE_ZA;

	/* Update PWR_MGMT_2 content if gyroscope if not used*/
	if (!use_gyr)
		dev.reg_val |= B_DISABLE_XG | B_DISABLE_YG | B_DISABLE_ZG;

	/* Update PWR_MGMT_2 */
	dev.reg = PWR_MGMT_2;
	i2c0_wReg(&dev);

	/* Set LPF bandwidth and sample frequency */
	switch (freq)
	{
		case 50:
			dlp_cfg = GM_DLPF_CFG_20Hz;
			smplrt_div = 20 - 1;
			break;
		case 100:
			dlp_cfg = GM_DLPF_CFG_41Hz;
			smplrt_div = 10 - 1;
			break;
		case 200:
			dlp_cfg = GM_DLPF_CFG_92Hz;
			smplrt_div = 5 - 1;
			break;
		case 250:
			dlp_cfg = GM_DLPF_CFG_92Hz;
			smplrt_div = 4 - 1;
			break;
		case 500:
			dlp_cfg = GM_DLPF_CFG_184Hz;
			smplrt_div = 2 - 1;
			break;
		case 1000:
			dlp_cfg = GM_DLPF_CFG_184Hz;
			smplrt_div = 1 - 1;
			break;
		default: success = false;
	}
	dev.reg = CONFIG;				// Set LPF bandwidth
	dev.reg_val = dlp_cfg;		// Set LPF bandwidth
	i2c0_wReg(&dev);				// Set LPF bandwidth
	dev.reg = ACCEL_CONFIG_2;	// Set LPF bandwidth
	i2c0_wReg(&dev);				// Set LPF bandwidth
	dev.reg = SMPLRT_DIV;		// Set sample frequency
	dev.reg_val = smplrt_div;	// Set sample frequency
	i2c0_wReg(&dev);				// Set sample frequency

	/* Set accelerometer range */
	switch (range_acc)
	{
		case 2: dev.reg_val = GM_ACCEL_FS_SEL_2g; break;
		case 4: dev.reg_val = GM_ACCEL_FS_SEL_4g; break;
		case 8: dev.reg_val = GM_ACCEL_FS_SEL_8g; break;
		case 16: dev.reg_val = GM_ACCEL_FS_SEL_16g; break;
		default: success = false;
	}
	dev.reg = ACCEL_CONFIG;
	i2c0_wReg(&dev);

	/* Set gyroscope range */
	switch (range_gyr)
	{
		case 250: dev.reg_val = GM_GYRO_FS_SEL_250dps; break;
		case 500: dev.reg_val = GM_GYRO_FS_SEL_500dps; break;
		case 1000: dev.reg_val = GM_GYRO_FS_SEL_1000dps; break;
		case 2000: dev.reg_val = GM_GYRO_FS_SEL_2000dps; break;
		default: success = false;
	}
	dev.reg = GYRO_CONFIG;
	i2c0_wReg(&dev);

	/* Check success */
	if (!success) {
		mpu9250_reset(dev);
		return false;
	}

	/* Leave the bypass enabled */
	mpu9250_enable_i2c_bypass(dev);

	/* Return success */
	return true;
}


/************************************************************************/
/* Read data                                                            */
/************************************************************************/
void mpu9250_read_acc(i2c_dev_t dev, uint8_t * acc)
{
	dev.reg = ACCEL_XOUT_H;
	i2c0_rReg(&dev, 6);
		#ifdef XMEGA
	memcpy(acc, (uint8_t *)(&dev.data), 6);
	#else
		*(acc+0) = dev.buff[0];
		*(acc+1) = dev.buff[1];
		*(acc+2) = dev.buff[2];
		*(acc+3) = dev.buff[3];
		*(acc+4) = dev.buff[4];
		*(acc+5) = dev.buff[5];
	#endif
}

void mpu9250_read_gyr(i2c_dev_t dev, uint8_t * gyr)
{
	dev.reg = GYRO_XOUT_H;
	i2c0_rReg(&dev, 6);
	#ifdef XMEGA
		memcpy(gyr, (uint8_t *)(&dev.data), 6);
	#else
		*(gyr+0) = dev.buff[0];
		*(gyr+1) = dev.buff[1];
		*(gyr+2) = dev.buff[2];
		*(gyr+3) = dev.buff[3];
		*(gyr+4) = dev.buff[4];
		*(gyr+5) = dev.buff[5];
	#endif
}

void mpu9250_read_mag(i2c_dev_t dev, uint8_t * mag)
{
	uint8_t mpu9250_add = dev.add;	// Save MPU9250 address
	dev.add = 0x0C;						// Set AK8975 address
	
	dev.reg = HXL;
	i2c0_rReg(&dev, 6);
	
	#ifdef XMEGA
		*(mag+0) = dev.data[1];
		*(mag+1) = dev.data[0];
		*(mag+2) = dev.data[3];
		*(mag+3) = dev.data[2];
		*(mag+4) = dev.data[5];
		*(mag+5) = dev.data[4];
	#else
		*(mag+0) = dev.buff[0];
		*(mag+1) = dev.buff[1];
		*(mag+2) = dev.buff[2];
		*(mag+3) = dev.buff[3];
		*(mag+4) = dev.buff[4];
		*(mag+5) = dev.buff[5];
	#endif
	
	dev.add = mpu9250_add;				// Recover MPU9250 address
}

void mpu9250_read_temp(i2c_dev_t dev, uint8_t * temp)
{
	dev.reg = TEMP_OUT_H;
	i2c0_rReg(&dev, 2);
	#ifdef XMEGA
		memcpy(temp, (uint8_t *)(&dev.data), 2);
	#else
		*(temp+0) = dev.buff[0];
		*(temp+1) = dev.buff[1];
	#endif
}