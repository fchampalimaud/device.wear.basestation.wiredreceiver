#ifndef _MPU9250_H_
#define _MPU9250_H_

#define XMEGA
//#define NRF

#ifdef XMEGA
	#include "i2c.h"
#else
	#include <stdbool.h>
	#include <stdint.h>
	#include "..\nRF24LE1\nRF_i2c.h"
#endif

#ifndef bool
	#define bool uint8_t
	#define true 1
	#define false 0
#endif

/************************************************************************/
/* Registers add                                                        */
/************************************************************************/
// Core, Acc & Gyro
#define SELF_TEST_X_GYRO   0
#define SELF_TEST_Y_GYRO   1
#define SELF_TEST_Z_GYRO   2
#define SELF_TEST_X_ACCEL  13
#define SELF_TEST_Y_ACCEL  14
#define SELF_TEST_Z_ACCEL  15

#define XG_OFFSET_H        19
#define XG_OFFSET_L        20
#define YG_OFFSET_H        21
#define YG_OFFSET_L        22
#define ZG_OFFSET_H        23
#define ZG_OFFSET_L        24

#define SMPLRT_DIV         25
#define CONFIG             26
#define GYRO_CONFIG        27
#define ACCEL_CONFIG       28
#define ACCEL_CONFIG_2     29
#define LP_ACCEL_ODR       30
#define WOM_THR            31
#define FIFO_EN            35

#define INT_PIN_CFG        55
#define INT_ENABLE         56
#define INT_STATUS         58

#define ACCEL_XOUT_H       59
#define ACCEL_XOUT_L       60
#define ACCEL_YOUT_H       61
#define ACCEL_YOUT_L       62
#define ACCEL_ZOUT_H       63
#define ACCEL_ZOUT_L       64
#define TEMP_OUT_H         65
#define TEMP_OUT_L         66
#define GYRO_XOUT_H        67
#define GYRO_XOUT_L        68
#define GYRO_YOUT_H        69
#define GYRO_YOUT_L        70
#define GYRO_ZOUT_H        71
#define GYRO_ZOUT_L        72

#define SIGNAL_PATH_RESET  104
#define MOT_DETECT_CTRL    105
#define USER_CTRL          106
#define PWR_MGMT_1         107
#define PWR_MGMT_2         108

#define FIFO_COUNTH        114
#define FIFO_COUNTL        115
#define FIFO_R_W           116
#define WHO_AM_I           117 // Should contain 0x71

// Magnetometer
#define WIA						0
#define INFO					1
#define ST1						2
#define HXL						3
#define ST2						9
#define CNTL1					10
#define CNTL2					11
#define ASTC					12
#define TS1						13
#define TS2						14
#define I2CDIS					15
#define ASAX					16
#define ASAY					17
#define ASAZ					18


/************************************************************************/
/* Register contents                                                    */
/************************************************************************/
// Core, Acc & Gyro
#define SMPLRT_DIV_val_8Kz_50Hz  	159
#define SMPLRT_DIV_val_8Kz_100Hz 	79
#define SMPLRT_DIV_val_8Kz_200Hz 	39
#define SMPLRT_DIV_val_1Kz_50Hz  	19
#define SMPLRT_DIV_val_1Kz_100Hz 	9
#define SMPLRT_DIV_val_1Kz_200Hz 	4

#define INT_ENABLE_msk_data_rdy		0x01	// All data is ready
#define INT_ENABLE_msk_i2c_mst		0x08	// I2C master interrupt
#define INT_ENABLE_msk_fifo_oflow	0x10	// FIFO achive overflow
#define INT_STATUS_msk_data_rdy		0x01	// All data is ready
#define INT_STATUS_msk_i2c_mst		0x08	// I2C master interrupt
#define INT_STATUS_msk_fifo_oflow	0x10	// FIFO achive overflow



/* CONFIG (26) */
/* ACCEL_CONFIG_2 (29) */
#define MSK_DLPF_CFG				(3<<0)	// Filter bandwidth
#define GM_DLPF_CFG_184Hz		(1<<0)
#define GM_DLPF_CFG_92Hz		(2<<0)		
#define GM_DLPF_CFG_41Hz		(3<<0)
#define GM_DLPF_CFG_20Hz		(4<<0)
#define GM_DLPF_CFG_10Hz		(5<<0)
#define GM_DLPF_CFG_5Hz			(6<<0)

/* GYRO_CONFIG (27) */
#define MSK_GYRO_FS_SEL				(3<<3)	// Gyro full scale select
#define GM_GYRO_FS_SEL_250dps		(0<<3)	// +/- 250 dps
#define GM_GYRO_FS_SEL_500dps		(1<<3)	// +/- 500 dps
#define GM_GYRO_FS_SEL_1000dps	(2<<3)	// +/- 1000 dps
#define GM_GYRO_FS_SEL_2000dps	(3<<3)	// +/- 2000 dps

/* ACCEL_CONFIG (28) */
#define MSK_ACCEL_FS_SEL			(3<<3)	// Gyro full scale select
#define GM_ACCEL_FS_SEL_2g			(0<<3)	// +/- 2 g
#define GM_ACCEL_FS_SEL_4g			(1<<3)	// +/- 4 g
#define GM_ACCEL_FS_SEL_8g			(2<<3)	// +/- 8 g
#define GM_ACCEL_FS_SEL_16g		(3<<3)	// +/- 16 g

/* INT_PIN_CFG (55) */
#define B_ACTL						(1<<7)	// If set, the logic level for INT pin is active low
#define B_OPEN						(1<<6)	// If set, INT pin is configured as open drain
#define B_LATCH_INT_EN			(1<<5)	// If set, INT pin level held until interrupt status is cleared
#define B_INT_ANYRD_2CLEAR		(1<<4)	// If set, interrupt status is cleared if any read operation is performed
#define B_ACTL_FSYNC				(1<<3)	// If set, the logic level for the FSYNC pin as an interrupt is active low
#define B_FSYNC_INT_MODE_EN	(1<<2)	// If set, this enables the FSYNC pin to be used as an interrupt.
#define B_BYPASS_EN				(1<<1)	// If set, the i2c_master interface pins(ES_CL and ES_DA) will go into �bypass mode�

/* INT_ENABLE (56) */
#define B_WOM_WN					(1<<6)	// If set, enable interrupt for wake on motion to propagate to interrupt pin
#define B_FIFO_OVERFLOW_EN		(1<<4)	// If set, enable interrupt for fifo overflow to propagate to interrupt pin.
#define B_FSYNC_INT_EN			(1<<3)	// If set, enable Fsync interrupt to propagate to interrupt pin
#define B_RAW_RDY_EN				(1<<0)	// If set, enable Raw Sensor Data Ready interrupt to propagate to interrupt pin

/* INT_STATUS (58) */
#define B_WOM_INT					(1<<6)	// If set, wake on motion interrupt occurred
#define B_FIFO_OVERFLOW_INT	(1<<4)	// If set, Fifo Overflow interrupt occurred
#define B_FSYNC_INT				(1<<3)	// If set, Fsync interrupt occurred
#define B_RAW_DATA_RDY_INT		(1<<0)	// If set, sensor Register Raw Data sensors are updated and Ready to be read

/* PWR_MGMT_1 (107) */
#define B_H_RESET				(1<<7)	// Reset device
#define B_SLEEP				(1<<6)	// Sleep mode
#define B_CYCLE				(1<<5)	// Cycle between sleep and single sample (if SLEEP and STANDBY are not set)
#define B_GYRO_STANDBY		(1<<4)	// Low power mode that allows quick enabling of gyros
#define B_PD_PTAT				(1<<3)	// Power down internal PTAT
#define MSK_CLKSEL			(7<<0)	// Clock source
#define GM_CLKSEL_INT20MHz	(0<<0)	// Internal 20 MHz
#define GM_CLKSEL_BEST		(1<<0)	// Selects the best available clock source
#define GM_CLKSEL_STOP		(7<<0)	// Stops the clock and keeps timing generator in reset

/* PWR_MGMT_2 (108) */
#define B_DISABLE_XA			(1<<5)	// Disable acc X
#define B_DISABLE_YA			(1<<4)	// Disable acc Y
#define B_DISABLE_ZA			(1<<3)	// Disable acc Z
#define B_DISABLE_XG			(1<<2)	// Disable gyro X
#define B_DISABLE_YG			(1<<1)	// Disable gyro Y
#define B_DISABLE_ZG			(1<<0)	// Disable gyro Z

/* WHO_AM_I (117) */
#define GM_WHO_AM_I			0x71		// Device ID

// Magnetometer
/* WIA (0) */
#define GM_WIA					0x48		// Device ID

/* ST1 (2) */
#define B_DRDY					(1<<0)	// Bit turns to "1" when data is ready
												// in single measurement mode or self-test mode

/* CNTL1 (10) */
#define MSK_MODE				0x0F		// Operating mode
#define GM_POWERDOWN			(0<<0)	// Power-down mode
#define GM_SINGLE_MEAS		(1<<0)	// Single measurement mode
#define GM_CONT_MODE1		(2<<0)	// Continuous measurement mode 1
#define GM_CONT_MODE2		(6<<0)	// Continuous measurement mode 2
#define GM_EXT_TRIGGER		(4<<0)	// External trigger measurement mode
#define GM_SELF_TEST			(8<<0)	// Self-test mode
#define GM_FUSE_ROM			(15<<0)	// Fuse ROM access mode
#define B_BIT					(1<<4)	// Output bit setting: "0": 14 bits, "1": 16 bits


/************************************************************************/
/* Enums                                                                */
/************************************************************************/
/*
typedef enum {
	MPU9250_FREQ_50Hz = SMPLRT_DIV_val_8Kz_50Hz,
	MPU9250_FREQ_100Hz = SMPLRT_DIV_val_8Kz_100Hz,
	MPU9250_FREQ_200Hz = SMPLRT_DIV_val_8Kz_200Hz,
} mpu9150_freq_t;

typedef enum {
	MPU9250_RANGE_ACC_2g = ACCEL_CONFIG_msk_2g,
	MPU9250_RANGE_ACC_4g = ACCEL_CONFIG_msk_4g,
	MPU9250_RANGE_ACC_8g = ACCEL_CONFIG_msk_8g,
	MPU9250_RANGE_ACC_16g = ACCEL_CONFIG_msk_16g,
} mpu9150_range_acc_t;

typedef enum {
	MPU9250_RANGE_GYR_250dps = GYRO_CONFIG_msk_250dps,
	MPU9250_RANGE_GYR_500dps = GYRO_CONFIG_msk_500dps,
	MPU9250_RANGE_GYR_1000dps = GYRO_CONFIG_msk_1000dps,
	MPU9250_RANGE_GYR_2000dps = GYRO_CONFIG_msk_2000dps,
} mpu9150_range_gyr_t;

typedef enum {
	MPU9250_RANGE_MAG_undefined,
} mpu9150_range_mag_t;

*/


/************************************************************************/
/* Prototypes                                                           */
/************************************************************************/
bool mpu9250_exist(i2c_dev_t dev);
void mpu9250_reset(i2c_dev_t dev);
void mpu9250_sleep(i2c_dev_t dev);
bool mpu9250_check_raw_data_rdy_int(i2c_dev_t dev);
void mpu9250_enable_i2c_bypass(i2c_dev_t dev);
void mpu9250_disable_i2c_bypass(i2c_dev_t dev);
void mpu9250_mag_trig_single_meas(i2c_dev_t dev);
bool mpu9250_mag_check_drdy(i2c_dev_t dev);

bool mpu9250_start(
	i2c_dev_t dev,
	uint16_t freq,
	uint8_t use_acc,
	uint8_t use_gyr,
	uint8_t use_mag,
	uint8_t range_acc,
	uint16_t range_gyr);

void mpu9250_read_acc(i2c_dev_t dev, uint8_t * acc);
void mpu9250_read_gyr(i2c_dev_t dev, uint8_t * gyr);
void mpu9250_read_mag(i2c_dev_t dev, uint8_t * mag);
void mpu9250_read_temp(i2c_dev_t dev, uint8_t * temp);


#endif /* _MPU9250_H_ */