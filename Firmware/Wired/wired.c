// Author: Champalimaud Foundation's Hardware platform

#include "cpu_1v1.h"
#define F_CPU 32000000
#include <util/delay.h>
#include "uart0.h"
#include "i2c.h"
#include "mpu9250.h"
#include "protocol.h"

// https://github.com/kriswiner/MPU-9250/blob/master/STM32F401/MPU9250.h
// https://github.com/kriswiner/MPU-9250/blob/master/STM32F401/main.cpp


/************************************************************************/
/* Firmware version                                                     */
/************************************************************************/
#define FW_VERSION_MAJOR 2
#define FW_VERSION_MINOR 1

/************************************************************************/
/* Hardware version                                                     */
/************************************************************************/
#define HW_VERSION_MAJOR 1
#define HW_VERSION_MINOR 2


/************************************************************************/
/*Globals and externs                                                   */
/************************************************************************/
/* Define MPU9250 */
i2c_dev_t mpu9250;

/* Commands */
wake_up_t wake_up;
stim3_t stim;

/* Stream of data */
data_16_bits_t frame;
tx_header_t frame_head;

/* USART RX buufer and pointer */
#if UART0_RXBUFSIZ > 256
	extern uint16_t uart0_rx_pointer;
#else
	extern uint8_t uart0_rx_pointer;
#endif
	extern uint8_t rxbuff_uart0[];

#define DAC_GAINCAL 0x83
#define DAC_OFFSETCAL 0x85

/************************************************************************/
/* Initialize pins                                                      */
/************************************************************************/
ISR(PORTD_INT0_vect, ISR_NAKED)
{
	wdt_reset_device();
}

#define set_LED clear_io(PORTD, 3)
#define clr_LED set_io(PORTD, 3)
#define tgl_LED toogle_io(PORTD, 3)

#define set_LED_STIM set_io(PORTD, 4)
#define clr_LED_STIM clear_io(PORTD, 4)
#define tgl_LED_STIM toogle_io(PORTD, 4)

#define en_LDO5V	set_io(PORTC, 3)
#define dis_LDO5V	clear_io(PORTC, 3)

#define en_LDO12V		set_io(PORTC, 4)
#define dis_LDO12V	clear_io(PORTC, 4)

#define en_IOFF	clear_io(PORTB, 3)
#define dis_IOFF	set_io(PORTB, 3)
#define read_IOFF   read_io(PORTB, 3)

#define set_SAMPLE set_io(PORTD, 0)
#define clr_SAMPLE clear_io(PORTD, 0)
#define tgl_SAMPLE toogle_io(PORTD, 0)

#define read_EN read_io(PORTD, 2)

void init_pins(void)
{
	/* Configure pins */
	io_pin2out(&PORTD, 3, OUT_IO_DIGITAL, IN_EN_IO_DIS);	// LED3
	clr_LED;
	io_pin2out(&PORTD, 4, OUT_IO_DIGITAL, IN_EN_IO_DIS);	// LED2
	clr_LED_STIM;
	io_pin2out(&PORTD, 0, OUT_IO_DIGITAL, IN_EN_IO_DIS);	// SAMPLE
	set_SAMPLE;
	io_pin2out(&PORTC, 3, OUT_IO_DIGITAL, IN_EN_IO_DIS);	// ENLDO5
	dis_LDO5V;
	io_pin2out(&PORTC, 4, OUT_IO_DIGITAL, IN_EN_IO_DIS);	// ENLDO12
	dis_LDO12V;
	io_pin2out(&PORTB, 3, OUT_IO_DIGITAL, IN_EN_IO_EN);	    // IOFF
	dis_IOFF;
	io_pin2in(&PORTD, 2, PULL_IO_TRISTATE, SENSE_IO_LOW_LEVEL);	// EN2
	io_set_int(&PORTD, INT_LEVEL_LOW, 0, (1<<2), true);			// EN2
	
	/* Check if wired is enabled */
	if (read_EN)
	{		
		io_pin2in(&PORTD, 2, PULL_IO_TRISTATE, SENSE_IO_LOW_LEVEL);	// EN2
		io_set_int(&PORTD, INT_LEVEL_LOW, 0, (1<<2), true);			// EN2
		
		SLEEP_CTRL = SLEEP_SMODE_PDOWN_gc | SLEEP_SEN_bm;
		cpu_enable_int_level(INT_LEVEL_LOW);
		
		__asm volatile("sleep");
	}
	else
	{	
		io_pin2in(&PORTD, 2, PULL_IO_TRISTATE, SENSE_IO_EDGE_RISING);	// EN2
		io_set_int(&PORTD, INT_LEVEL_LOW, 0, (1<<2), true);				// EN2
	}

	/* Power up devices */
	set_LED;
	en_LDO5V;
	en_LDO12V;

	/* DAC start-up time (precaution only) */
	_delay_ms(1);

	/* Check if the calibration exist */
	if (eeprom_rd_byte(0) == 0xAA && eeprom_rd_byte(1) == 0xBB)
	{
		DACB.CH0GAINCAL = eeprom_rd_byte(2);
		DACB.CH0OFFSETCAL = eeprom_rd_byte(3);
	}
	else
	{
		/* Configure using default values */
		DACB.CH0GAINCAL = DAC_GAINCAL;
		DACB.CH0OFFSETCAL = DAC_OFFSETCAL;
	}

	/* Initialize DACB channel 0 */
	DACB.CTRLB = 0;
	DACB.CTRLC = DAC_REFSEL_AREFA_gc;
	DACB.CTRLA = DAC_CH0EN_bm | DAC_ENABLE_bm;
}


/************************************************************************/
/* main()                                                               */
/************************************************************************/
int main(void)
{
	/* Disable watchdog */
	wdt_disable();

	/* Configure CLK */
	cpu_config_clock(F_CPU, true, true);
	
	/* Init pins */
	init_pins();

	/* Tests */
	//r = start_acquisition();

	/* Initialize UART0 with 500 Kbit/s*/
	/* Wired */
	uart0_init(3, 0, false);
	uart0_enable();

	/* Initialize up frame */
	frame.cmd = 0;
	frame.counter = 0;
	frame.id = 0;

	/* Initialize up frame header */
	frame_head.header[0] = 'w';
	frame_head.header[1] = '>';
	frame_head.header[2] = ':';
	
	/* Define sleep mode */
	SLEEP_CTRL = SLEEP_SMODE_IDLE_gc | SLEEP_SEN_bm;

	/* Enable interrupts */
	cpu_enable_int_level(INT_LEVEL_LOW);
	cpu_enable_int_level(INT_LEVEL_MED);
	cpu_enable_int_level(INT_LEVEL_HIGH);
	
	/* Infinite loop */
	while(1)
	;//__asm volatile("sleep");

}


/************************************************************************/
/* UART RX routine (high level interrupt)                               */
/************************************************************************/
uint8_t cmd_len;
volatile bool cmd_received;
volatile bool acquiring = false;

void execute_command (void);

void uart0_rcv_byte_callback(uint8_t byte)
{	
	if (!uart0_rx_pointer)
	{
		if ((byte & HEADER_MASK) == GM_HEADER)
		{
			cmd_received = false;

			*(rxbuff_uart0) = byte;
			uart0_rx_pointer++;
			
			switch (byte)
			{
				case CMD_WAKE_UP:
					cmd_len = LEN_WAKE_UP;
					break;
				case CMD_DISABLE_RX:
					cmd_len = LEN_DISABLE_RX;
					break;
				case CMD_STIM_3:
					cmd_len = LEN_STIM_3;;
					break;
				default:
					uart0_rx_pointer = 0;
			}
		}
	}
	else
	{
		*(rxbuff_uart0 + uart0_rx_pointer) = byte;
		
		if (++uart0_rx_pointer == cmd_len)
		{
			uart0_rx_pointer = 0;

			if (!acquiring && rxbuff_uart0[0] == CMD_WAKE_UP)
			{
				disable_uart0_rx;
				execute_command();	
				enable_uart0_rx;
			}
            /* if not acquiring motion frames and the command is a stimulation, execute immediately */
            else if (!acquiring && rxbuff_uart0[0] == CMD_STIM_3)
            {   
				disable_uart0_rx;
				execute_command();	
				enable_uart0_rx;
            }                            
			else
            {
				cmd_received = true;
            }                
		}
	}
}

/************************************************************************/
/* Execute the commands                                                 */
/************************************************************************/
bool start_acquisition(void);
void stop_acquisition(void);
void update_metadata(void);
bool configure_stimulation (void);
void start_stimulation (void);

bool use_LED;

bool send_stimulation = false;

uint8_t stimulation_acknowledge[5] = {'a', 'a', 'a', 'a', 'a'};

#if UART0_TXBUFSIZ > 256
    extern uint16_t uart0_tail;
    extern uint16_t uart0_head;
#else
    extern uint8_t uart0_tail;
    extern uint8_t uart0_head;
#endif


void execute_command (void)
{
	/* Stop stimulation when if receive any command */
	dis_IOFF;
	timer_type0_stop(&TCD0);
	clr_LED_STIM;
	
	switch(*(rxbuff_uart0))
	{
		/* Wake up */
		case CMD_WAKE_UP:
			wake_up = *((wake_up_t*)(rxbuff_uart0));

			use_LED = (wake_up.freq & 0x80) ? true : false;
			wake_up.freq &= 0x1F;

			if (!start_acquisition())
				for (uint8_t i = 0; i < 20 ; i++)
				{
					if (use_LED)
						tgl_LED;
					_delay_ms(100);
				}
					
			break;
				
		/* Disable rx */
		case CMD_DISABLE_RX:
			stop_acquisition();

			break;
				
		/* Stimulation */
		case CMD_STIM_3:
			stim = *((stim3_t*)rxbuff_uart0);
					
			if (configure_stimulation())
            {
                send_stimulation = true;
                
                /* Stimulate immediately if the device is not acquiring motion frames */
                if (!acquiring)
                {
                    /* Wait until the master is ready to receive the acknowledge */
                    while (read_io(UART0_CTS_PORT, UART0_CTS_pin) != false);
                    
                    /* If tx buffer is empty */
                    if (uart0_tail == uart0_head)
                    {
                        disable_uart0_rx;
                        send_stimulation = false;
                        uart0_xmit(stimulation_acknowledge, 5);
                        start_stimulation();
                        enable_uart0_rx; 
                    }               
                }
            }

			break;
				
		/* stim */
		/*
		case 0x62:
		checksum = 0;
		for (;pointer < 7; pointer ++)
		checksum += uart_rx[pointer];
		pointer = 0;
				
		if (uart_rx[7] == checksum)
		{
			stim = *((wake_up_t*)(uart_rx));
			stim_to_send = true;
		}
		break;
		*/
	}
}


/************************************************************************/
/* Wake up: Starts the 9AXIS acquisition                                */
/************************************************************************/
uint16_t led_tgl_counter;
uint16_t led_tgl_counter_target;

uint8_t mag_100Hz_counter;
uint8_t mag_100Hz_counter_target;

bool start_acquisition(void)
{
	/* Create parameters */
	uint16_t freq;
	uint8_t range_xyz;
	uint16_t range_gyr;

	/* Check if the necessary bandwidth is available */
	if (wake_up.freq == GM_FRQ_1000Hz)
	{
		if ((wake_up.id_chmask & B_USE_ACC) && (wake_up.id_chmask & B_USE_GYR))
			return false;
		if ((wake_up.id_chmask & B_USE_ACC) && (wake_up.id_chmask & B_USE_MAG))
			return false;		
		if ((wake_up.id_chmask & B_USE_GYR) && (wake_up.id_chmask & B_USE_MAG))
			return false;
	}
	
	/* Magnetometer can't work alone */
	if (!(wake_up.id_chmask & B_USE_ACC) && !(wake_up.id_chmask & B_USE_GYR) && (wake_up.id_chmask & B_USE_MAG))
		return false;

	/* Reset sensors data and the counter */
	frame.counter = 0;
	for (uint8_t i = 0; i < 3; i++)
	{
		frame.data_sensor1[i] = 0;
		frame.data_sensor2[i] = 0;
		frame.data_sensor3[i] = 0;
	}

	/* Load parameters: sample frequency and 100Hz counters */
	switch (wake_up.freq)
	{
		case GM_FRQ_50Hz: freq = 50; mag_100Hz_counter_target = 1; break;
		case GM_FRQ_100Hz: freq = 100; mag_100Hz_counter_target = 1; break;
		case GM_FRQ_200Hz: freq = 200; mag_100Hz_counter_target = 2; break;
		case GM_FRQ_250Hz: freq = 250; mag_100Hz_counter_target = 3; break;
		case GM_FRQ_500Hz: freq = 500; mag_100Hz_counter_target = 5; break;
		case GM_FRQ_1000Hz: freq = 1000; mag_100Hz_counter_target = 10; break;
		default: return false;
	}

	/* Calculate led and mag 100Hz counters and targets */
	led_tgl_counter = 0;
	led_tgl_counter_target = freq / 2;
	mag_100Hz_counter = 0;
	
	/* Load parameters: accelerometer range */
	switch (wake_up.range_xyz)
	{
		case GM_RANGE_XYZ_2g: range_xyz = 2; break;
		case GM_RANGE_XYZ_4g: range_xyz = 4; break;
		case GM_RANGE_XYZ_8g: range_xyz = 8; break;
		case GM_RANGE_XYZ_16g: range_xyz = 16; break;
		default: return false;
	}

	/* Load parameters: gyroscope range */
	switch (wake_up.range_gyr)
	{
		case GM_RANGE_GYR_250dps: range_gyr = 250; break;
		case GM_RANGE_GYR_500dps: range_gyr = 500; break;
		case GM_RANGE_GYR_1000dps: range_gyr = 1000; break;
		case GM_RANGE_GYR_2000dps: range_gyr = 2000; break;
		default: return false;
	}
	
	/* Initialize MPU9250 address */
	mpu9250.add = 0x68;

	/* Initialize I2C here to toggle the lines */
	i2c0_init();

	/* Check if the device exist on the bus */
	if (!mpu9250_exist(mpu9250))
		return false;
	
	/* Start acquisition */
	bool start;
	start = mpu9250_start(	mpu9250,
									freq,
									(wake_up.id_chmask & B_USE_ACC),
									(wake_up.id_chmask & B_USE_GYR),
									(wake_up.id_chmask & B_USE_MAG),
									range_xyz,
									range_gyr);
	if (!start)
		return false;
	
	/* Launch timer to check for data (32us) */
	timer_type0_enable(&TCC0, TIMER_PRESCALER_DIV256, 4, INT_LEVEL_LOW);

	/* Return */
	acquiring = true;
	clr_LED;
	return true;
}

/************************************************************************/
/* Disable rx: Starts the 9AXIS acquisition                             */
/************************************************************************/
void stop_acquisition(void)
{
	acquiring = false;
	
	cpu_disable_int_level(INT_LEVEL_MED);
	mpu9250_reset(mpu9250);
	cpu_enable_int_level(INT_LEVEL_MED);

	timer_type0_stop(&TCC0);
	if (use_LED)
		set_LED;
}

/************************************************************************/
/* Acquisition timer (low level interrupt)                              */
/************************************************************************/
uint8_t checksum;
uint8_t stim_counter;

uint8_t device_temperature[2];

ISR(TCC0_OVF_vect, ISR_NAKED)
{	
	/* Check if a command was received */
	cpu_disable_int_level(INT_LEVEL_HIGH);
	cpu_disable_int_level(INT_LEVEL_MED);
	
    if (cmd_received)
	{
		disable_uart0_rx;
        cmd_received = false;
		execute_command();
		enable_uart0_rx;
	}
    
    if (send_stimulation)
    {
        /* Start stimulation if the master is able to receive the stimulation_aknowledge */
        if (read_io(UART0_CTS_PORT, UART0_CTS_pin) ? false : true)
        {
            if (uart0_tail == uart0_head)
            {
                disable_uart0_rx;
                send_stimulation = false;
                uart0_xmit(stimulation_acknowledge, 5);
                start_stimulation();
		        enable_uart0_rx;
            }                
        }
        else
        {
            clr_LED_STIM;
        }            
    }        
	cpu_enable_int_level(INT_LEVEL_MED);
	cpu_enable_int_level(INT_LEVEL_HIGH);
	
	
	/* Disable stimulation interupt every time it communicates with the accelerometer */
	cpu_disable_int_level(INT_LEVEL_MED);
	_delay_us(50);

	if (mpu9250_check_raw_data_rdy_int(mpu9250))
	{
		cpu_enable_int_level(INT_LEVEL_MED);
		
		/* Notify master that a sample arrived */
		clr_SAMPLE;

		/*
		cpu_disable_int_level(INT_LEVEL_MED);
		*/
        
		/* Handle magnetometer */
		if (wake_up.id_chmask & B_USE_MAG)
		{
			if (++mag_100Hz_counter == mag_100Hz_counter_target)
			{
				cpu_disable_int_level(INT_LEVEL_MED);
				_delay_us(50);
				mpu9250_read_mag(mpu9250, (uint8_t *)(&frame.data_sensor3));
				cpu_enable_int_level(INT_LEVEL_MED);

				cpu_disable_int_level(INT_LEVEL_MED);
				_delay_us(50);
				mpu9250_mag_trig_single_meas(mpu9250);
				mpu9250_read_temp(mpu9250, device_temperature);
				cpu_enable_int_level(INT_LEVEL_MED);
				
				mag_100Hz_counter = 0;
			}
		}
		else
		{
			cpu_disable_int_level(INT_LEVEL_MED);
			_delay_us(50);
			mpu9250_read_temp(mpu9250, device_temperature);
			cpu_disable_int_level(INT_LEVEL_MED);
		}
		
		/* Update metadata with the right data */
		if (frame.cmd == 0x0C)
		{
			if (frame.metadata == frame.counter)
				frame.cmd++;
			frame.metadata = stim_counter;
		}
		if (frame.cmd == 0x0E)
		{
			frame.cmd = 0x12;
		}
		update_metadata();
        
        //frame.data_sensor3[0] = ((uint16_t)frame.metadata) << 8;
        //frame.data_sensor3[1] = ((uint16_t)frame.cmd) << 8;
        //frame.data_sensor3[2] = ((uint16_t)frame.counter) << 8;
		
		/* Collect data */
		if (wake_up.id_chmask & B_USE_ACC)
		{
			cpu_disable_int_level(INT_LEVEL_MED);
			_delay_us(50);
			mpu9250_read_acc(mpu9250, (uint8_t *)(&frame.data_sensor1));
			cpu_enable_int_level(INT_LEVEL_MED);
		}
		if (wake_up.id_chmask & B_USE_GYR)
		{
			cpu_disable_int_level(INT_LEVEL_MED);
			_delay_us(50);
			mpu9250_read_gyr(mpu9250, (uint8_t *)(&frame.data_sensor2));
			cpu_enable_int_level(INT_LEVEL_MED);
		}
		
        /* Update notification that the stimulation LED is ON */
        if (!read_IOFF)
        {
            frame.counter |= 0x80;
        }
        
		/* Send frame header and packet */
		cpu_disable_int_level(INT_LEVEL_HIGH);
		uart0_xmit((uint8_t*)(&frame_head), 3);
		uart0_xmit(&(frame.cmd), 22);
		cpu_enable_int_level(INT_LEVEL_HIGH);
		
		/* Calculate checksum */
		checksum = 0;
		for (uint8_t i = 0; i < LEN_DATA_16BITs; i++)
			checksum += *(((uint8_t *)(&frame)) + i);
		
        /* Remove notification that the stimulation LED is ON */
        frame.counter &= ~(0x80);
        
		/* Send check sum */
		cpu_disable_int_level(INT_LEVEL_HIGH);
		uart0_xmit((&checksum), 1);
		cpu_enable_int_level(INT_LEVEL_HIGH);

		/* Update counter and command index */
		if (frame.cmd != 0x0C)
		{			
			if (++frame.cmd == 0x15 + 1)	// Reset command
				frame.cmd = 0;
			if (frame.cmd == 0x0C)			// Skip 0x0C command
				frame.cmd++;
		}
			
		if (++frame.counter == 128)
            frame.counter = 0;
		
		/* Toggle LED */
		if (++led_tgl_counter == led_tgl_counter_target)
		{
			led_tgl_counter = 0;
			if (use_LED)
				tgl_LED;
		}

		/* Remove notification */
		set_SAMPLE;
	}
	cpu_enable_int_level(INT_LEVEL_MED);
	
	/* Re-launch timer to check for data (8us) */
	if (acquiring)
		timer_type0_enable(&TCC0, TIMER_PRESCALER_DIV256, 1, INT_LEVEL_LOW);
	
	/* Return */
	reti();
}


/************************************************************************/
/* Metadata                                                             */
/************************************************************************/
void update_metadata(void)
{
	if (frame.cmd == 0x00)
	{
		frame.metadata = 0;
	}

	else if (frame.cmd == 0x01)
	{
		frame.metadata = 129;
	}

	else if (frame.cmd == 0x02)
	{
		frame.metadata = 1;
	}
	
	else if (frame.cmd == 0x03)
	{
		frame.metadata = *(((uint8_t*)(&device_temperature)) + 0);
	}
	
	else if (frame.cmd == 0x04)
	{
		frame.metadata = wake_up.freq;
	}
	
	else if (frame.cmd == 0x05)
	{
		frame.metadata = 3;
	}
		
	else if (frame.cmd == 0x06)
	{
		frame.metadata = 0;
	}
	
	else if (frame.cmd == 0x07)
	{
		frame.metadata = wake_up.range_xyz;
	}
		
	else if (frame.cmd == 0x08)
	{
		frame.metadata = wake_up.range_gyr;
	}	

	else if (frame.cmd == 0x09)
	{
		frame.metadata = 0;
	}
	
	else if (frame.cmd == 0x0A)
	{
		frame.metadata = wake_up.id_chmask & (B_USE_ACC | B_USE_GYR | B_USE_MAG);
	}

	else if (frame.cmd == 0x0B)
	{
		frame.metadata = 100;
	}	
	
	else if (frame.cmd == 0x0C)
	{
		//frame.metadata = stim.id;
	}
	
	else if (frame.cmd == 0x0D)
	{
		frame.metadata = 100;
	}

	else if (frame.cmd == 0x0E)
	{
		frame.metadata = 0;
	}
	else if (frame.cmd == 0x0F)
	{
		frame.metadata = 0;
	}
	else if (frame.cmd == 0x10)
	{
		frame.metadata = 0;
	}
	else if (frame.cmd == 0x11)
	{
		frame.metadata = 0;
	}	

	else if (frame.cmd == 0x12)
	{
		frame.metadata = FW_VERSION_MAJOR;
	}
	else if (frame.cmd == 0x13)
	{
		frame.metadata = FW_VERSION_MINOR;
	}
	else if (frame.cmd == 0x14)
	{
		frame.metadata = HW_VERSION_MAJOR;
	}
	else if (frame.cmd == 0x15)
	{
		frame.metadata = HW_VERSION_MINOR;
	}
}


/************************************************************************/
/* Stimulation                                                          */
/************************************************************************/
uint16_t on_ms, off_ms, repetitions;

bool configure_stimulation (void) {
	/* Calculate checksum */
	uint8_t check = 0;
	for (uint8_t i = 0; i < LEN_STIM_3 - 1; i++)
		check += *(((uint8_t *)(&rxbuff_uart0)) + i);

	/* Make sure the timer is off and the stimulation current is off */
	timer_type0_stop(&TCD0);
	dis_IOFF;

	/* Return if checksum is not valid */
	if (check != stim.checksum)
		return false;

	/* Check data boundaries */
	if (stim.ms_on < 1 || stim.ms_on > 60000 )
		return false;
	if (stim.ms_off < 1 || stim.ms_off > 60000 )
		return false;
	if (stim.repetitions < 1 || stim.repetitions > 60000 )
		return false;
        
    if (stim.current < 10)
        stim.current = 10;
        
    if (stim.current > 500)
        stim.current = 500;

	/* Initialize counters */
	on_ms = 0;
	off_ms = 0;
	repetitions = 0;

	/* Set DAC value */
	DACB_CH0DATA = (uint16_t) ( (float)(stim.current) * ((2.0 / 1000.0) / (2.048 / 4096.0)) );

	/* Return true */
	return true;
}

void start_stimulation (void)
{
	timer_type0_enable(&TCD0, TIMER_PRESCALER_DIV256, 125, INT_LEVEL_MED);
	
	//frame.cmd = 0x0C;
	stim_counter = frame.counter;
	frame.metadata = frame.counter + 1;

	if (use_LED)
		set_LED_STIM;
	en_IOFF;
}

/************************************************************************/
/* Stimulation timer (medium level interrupt)                           */
/************************************************************************/
// Takes less than 3 us to run

ISR(TCD0_OVF_vect)
{
	if (++on_ms >= stim.ms_on)
	{
		dis_IOFF;

		on_ms--;
		if (++off_ms >= stim.ms_off + 1) 
		{
			if (++repetitions == stim.repetitions)
			{
				timer_type0_stop(&TCD0);
				clr_LED_STIM;
				return;
			}

			en_IOFF;

			on_ms = 0;
			off_ms = 0;
		}
	}
}